package apriliana.risqy.appxx6a

import android.database.sqlite.SQLiteDatabase
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), BottomNavigationView.OnNavigationItemSelectedListener {

    lateinit var  db : SQLiteDatabase
    lateinit var fragProdi : FragmentProdi
    lateinit var fragMhs : FragmentMahasiswa
    lateinit var fragPerkalian : FragmentPerkalian
    lateinit var fragPembagian : FragmentPembagian
    lateinit var ft : FragmentTransaction

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        bottomNavigationView.setOnNavigationItemSelectedListener(this)
        fragProdi = FragmentProdi()
        fragMhs = FragmentMahasiswa()
        fragPerkalian = FragmentPerkalian()
        fragPembagian = FragmentPembagian()
        db = DBOpenHelper(this).writableDatabase
    }

    fun getDbObject() : SQLiteDatabase{
        return db
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.itemProdi ->{
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.FrameLayout,fragProdi).commit()
                FrameLayout.setBackgroundColor(Color.argb(245,255,255,225))
                FrameLayout.visibility = View.VISIBLE
            }
            R.id.itemMhs ->{
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.FrameLayout,fragMhs).commit()
                FrameLayout.setBackgroundColor(Color.argb(245,255,225,255))
                FrameLayout.visibility = View.VISIBLE
            }
            R.id.itemPerkalian ->{
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.FrameLayout,fragPerkalian).commit()
                FrameLayout.setBackgroundColor(Color.argb(255,245,245,215))
                FrameLayout.visibility = View.VISIBLE
            }
            R.id.itemPembagian ->{
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.FrameLayout,fragPembagian).commit()
                FrameLayout.setBackgroundColor(Color.argb(255,245,245,215))
                FrameLayout.visibility = View.VISIBLE
            }
            R.id.itemAbout -> FrameLayout.visibility = View.GONE

        }
        return true
    }
}
